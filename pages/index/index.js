import http from "/common/request";
import Api from "/common/api";
import { open } from '/common/utils'
import bury from "/common/bury"

const SIZE = 8;
const globalData = getApp().globalData
Page({
  data: {
    helpShow: false,
    hotList: [],
    hotSearch: [],
    pageIndex: 0,
    pageSize: 8,
    showBanner: false
  },
  ...bury.call(this, ({
    event: 'PageView',
    page_name: '产业链图谱首页',
    content_id: 'P000001',
    content_title: '产业链图谱',
    stock_code: '',
    url: 'pages/index/index'
  })),
  onLoad(query) {
    this.getHotList();
    http.get(Api.getHotCompanys, { userId: true }).then(res => {
      this.setData({
        hotSearch: res.data
      });
    });

    // banner
    my.getAuthCode({
      scopes: "auth_base",
      success: (r) => {
        // test
        // const url = 'https://lizhilicai.guosen.com.cn/gs-alipybb-web/newstock/getIndustryChainQualifiedCustomer/1.0';
        // prod
        const url = 'https://tp.guosen.com.cn/gs-alipybb-web/newstock/getIndustryChainQualifiedCustomer/1.0'
        http.post(url, { code: r.authCode }).then(res => {
          console.log(res)
          // TODO: 是否显示banner wkfFlag 0 为不显示 ， 1为显示
          // 测试的话可以直接用0，而不是1
          // 发布到生产记得改回1
          if (res.result && res.result.wkfFlag === '1') { // '1'
            console.log('result')
            // 展示banner
            this.setData({
              showBanner: true
            });
          }
        });
      }
    });
  },
  onReady() {
    // 页面加载完成
  },
  getHotList() {
    const { pageIndex: index, pageSize: size } = this.data;
    http.get(Api.getHotList, { userId: true, index, size }).then(res => {
      this.setData({
        hotList: res.data
      });
    });
  },
  changeHot() {
    let { pageIndex, pageSize, hotList } = this.data;
    pageIndex += 1;
    if (pageIndex > SIZE || hotList.length !== pageSize) {
      pageIndex = 0;
    }
    this.setData({
      pageIndex
    });
    this.getHotList();
  },
  goSearch() {
    getApp().sensors.track('PageClick', {
      first_module: '首页',
      second_module: '产业链图谱',
      third_module: '产业链图谱',
      page_name: '产业链图谱首页',
      content_id: 'P000001',
      content_title: '产业链图谱',
      page_version: '20201230',
      button_area: '顶部',
      button_name: '搜索',
      button_id: 'B00000002',
      button_title: '搜索',
      stock_code: '',
      operating_activitie_name: '',
      operating_activitie_code: '',
      url: 'pages/index/index',
    });
    open({ url: "/pages/search/search" });
  },
  goHelper() {
    open({ url: "/pages/helper/helper" });
  },
  showHelp() {
    this.setData({
      helpShow: true
    });
  },
  setShow(flag) {
    this.setData({
      helpShow: flag
    });
  },
  goIndustry(e) {
    const { productName, productCode, increase } = e.target.dataset.item;
    // 
    getApp().sensors.track('PageClick', {
      first_module: '首页',
      second_module: '产业链图谱',
      third_module: '产业链图谱',
      page_name: '产业链图谱首页',
      content_id: 'P000001',
      content_title: '产业链图谱',
      page_version: '20201230',
      button_area: '热门产业',
      button_name: '产业详情',
      button_id: 'B00000001',
      button_title: productName,
      stock_code: '',
      operating_activitie_name: '',
      operating_activitie_code: '',
      url: 'pages/index/index',
    });
    globalData.navList.splice(0, globalData.navList.length)
    globalData.navList.push(e.target.dataset.item)
    open({
      url: `/pages/industry/industry?productName=${productName}&productCode=${productCode}&increase=${increase}`
    });
  },
  goStock(e) {
    const { securityName, securityCode } = e.target.dataset.item;
    getApp().sensors.track('PageClick', {
      first_module: '首页',
      second_module: '产业链图谱',
      third_module: '产业链图谱',
      page_name: '产业链图谱首页',
      content_id: 'P000001',
      content_title: '产业链图谱',
      page_version: '20201230',
      button_area: '多人在搜',
      button_name: '股票列表',
      button_id: 'B00000003',
      button_title: `${securityName}-${securityCode}`,
      stock_code: securityCode,
      operating_activitie_name: '',
      operating_activitie_code: '',
      url: 'pages/index/index',
    });
    globalData.navList.splice(0, globalData.navList.length)
    // globalData.navList.push(e.target.dataset.item)
    open({
      url: `/pages/stock/stock?securityName=${securityName}&securityCode=${securityCode}`
    });
  },
  onShareAppMessage() {
    // 返回自定义分享信息
    return {
      title: "My App",
      desc: "My App description",
      path: "pages/index/index"
    };
  },
  goLifestyle() {
    getApp().sensors.track('PageClick', {
      first_module: '首页',
      second_module: '产业链图谱',
      third_module: '产业链图谱',
      page_name: '产业链图谱首页',
      content_id: 'P000001',
      content_title: '产业链图谱',
      // page_version: '20201230',
      button_area: '查看生活号首页',
      button_name: '查看',
      button_id: 'B00000005',
      button_title: '',
      stock_code: '',
      operating_activitie_name: '',
      operating_activitie_code: '',
      url: 'pages/index/index',
    });
  },
  handleGoOther() {
    getApp().sensors.track('PageClick', {
      first_module: '首页',
      second_module: '产业链图谱',
      third_module: '产业链图谱',
      page_name: '支付宝产业链小程序首页',
      content_id: 'W20220208_ZFB_CYL_P1',
      content_title: '产业链图谱',
      page_version: '20220208',
      button_area: '支付宝产业链小程序首页－开户按钮',
      button_name: '去开户',
      button_id: 'D20220208_B1',
      stock_code: '',
      operating_activitie_name: '',
      operating_activitie_code: '',
      demand_describe: '统计产业链小程序首页－banner上的开户按钮点击情况',
      custom_key: '渠道名称',
      custom_value: '支付宝小程序',
      url: 'pages/index/index',
    });
    // my.ap.navigateToAlipayPage({
    //   path: 'alipays://platformapi/startapp?appClearTop=false&appId=20000067&instId=GXZQ&tradeSource=WSHOP&bizScenario=GXZQCaiFuHaoArticle&reCheck=YES&pd=NO&abv=NO&url=https://render.alipay.com/p/c/stocktrade/sign2.html',
    //   success: () => {

    //   },
    //   fail: () => {

    //   }
    // })
  }
});

// my.ap.navigateToAlipayPage
Page({
  data: {
    selectIndex: 0,
    lstart: 0,
    lend: 0,
    rstart: 0,
    rend: 0
  },
  onLoad() {
    this.setLine()
  },
  onHandle(e) {
    this.setData({
      selectIndex: e.target.dataset.index
    })
    this.setLine()
  },
  setLine() {
    my.createSelectorQuery()
      .select('.left .item:first-child').boundingClientRect()
      .select('.left .item:last-child').boundingClientRect()
      .select('.center .item.active').boundingClientRect()
      .select('.right .item:first-child').boundingClientRect()
      .select('.right .item:last-child').boundingClientRect()
      .exec((ret => {
        // const offset = ret[0].top - 30
        const arr1 = ret.slice(0, 3).map(item => (item.top + item.bottom) / 2).sort((a, b) => a - b)
        const arr2 = ret.slice(-3).map(item => (item.top + item.bottom) / 2).sort((a, b) => a - b)
        this.setData({ lstart: arr1[0], lend: arr1[arr1.length - 1], rstart: arr2[0], rend: arr2[arr2.length - 1] })
      }))
  }
});

import http from "/common/request";
import Api from "/common/api";
import { open } from '/common/utils'
let globalData = getApp().globalData
import bury from "/common/bury"

Page({
  data: {
    navList: [],
    companyList: [],
    isExpand: false,
    countLimit: 5,
    productName: "",
    increase: '',
    upAndDownData: null
  },
  onHide() {
    this._onHide && this._onHide();
    globalData.navList.splice(0, globalData.navList.length)
    this.setData({
      navList: globalData.navList
    });
    console.log(globalData.navList)
  },
  onUnload() {
    this._onUnload && this._onUnload();
    globalData.navList.splice(0, globalData.navList.length)
    this.setData({
      navList: globalData.navList
    });
  },
  onLoad(params) {
    const { productCode = "1140012344", productName = "消泡剂", increase } = params;

    const { onShow, onHide, onUnload } = bury.call(this, {
      event: 'PageView',
      page_name: '产业-产业链图谱',
      content_id: 'P000003',
      content_title: `${productName}-产业链图谱`,
      stock_code: '',
      url: 'pages/industry/industry'
    })

    this.onShow = onShow
    this._onHide = onHide
    this._onUnload = onUnload

    my.setNavigationBar({
      title: productName + '-产业链图谱'
    });
    let len = globalData.navList.length
    let _navList = [...globalData.navList]

    if (len > 5) {
      globalData.navList = globalData.navList.slice(-6)
      _navList = [...globalData.navList.slice(1, 5), { productName: '...' }, ...globalData.navList.slice(5)]
    }
    this.setData({
      navList: _navList,
      productName,
      increase,
      upAndDownHandle: this.upAndDownHandle.bind(this)
    });
    this.fetchData(productCode);
  },
  navTap(e) {
    const { productCode, productName, increase, securityCode, securityName } = e.target.dataset.item;
    if (e.target.dataset.index === globalData.navList.length - 1) return
    if (!securityCode && !productCode) return
    if (securityCode) {
      this.goStock(e)
    } else if (productCode) {
      this.upAndDownHandle(e)
    }
    // let len = globalData.navList.length
    // let _navList = [...globalData.navList]
    // if (len > 5) {
    //   globalData.navList = globalData.navList.slice(-6)
    //   _navList = [...globalData.navList.slice(0, 4), { productName: '...' }, ...globalData.navList.slice(5)]
    // }
    // this.setData({
    //   navList: _navList
    // })
  },
  upAndDownHandle(e) {
    const { productCode, productName, increase } = e.target.dataset.item;
    my.setNavigationBar({
      title: productName + '-产业链图谱'
    });
    globalData.navList.push(e.target.dataset.item);
    let len = globalData.navList.length
    let _navList;
    if (len > 5) {
      globalData.navList.splice(4,1)
      _navList = [...globalData.navList.slice(0, 4), { productName: '...' }, ...globalData.navList.slice(4)]
    } else {
      _navList = [...globalData.navList]
    }
    this.setData({
      navList: _navList,
      increase
      // productName
    });
    this.fetchData(productCode, productName);
  },
  fetchData(productCode, productName) {
    this.getRelatedCompanys(productCode);
    this.getUpAndDownChain(productCode, productName);
  },
  getUpAndDownChain(productCode, productName) {
    http
      .get(Api.getUpAndDownChain, {
        productCode,
        userId: true
      })
      .then(res => {
        this.setData({
          upAndDownData: res.data || {},
          productName
        });
      });
  },
  getRelatedCompanys(productCode) {
    http
      .get(Api.getRelatedCompanys, { userId: true, productCode })
      .then(res => {
        let companyList = res.data;
        companyList = companyList.map(item => {
          const increase = Number(item.increase);
          if (increase > 0) {
            item.color = "red";
          }
          if (increase < 0) {
            item.color = "green";
          }
          if (increase == 0) {
            item.color = "normal";
          }
          return item;
        });
        this.setData({
          companyList
        });
      });
  },
  expand() {
    this.setData({
      isExpand: !this.data.isExpand
    });
  },
  goHome() {
    my.redirectTo({
      url: "/pages/index/index"
    });
  },
  goStock(e) {
    const { securityName, securityCode } = e.target.dataset.item;
    globalData.navList.push(e.target.dataset.item)
    open({ url: `/pages/stock/stock?securityName=${securityName}&securityCode=${securityCode}` })
  },
  goMarket(e) {
    const { securityName, securityCodeM } = e.target.dataset.item;
    const [code, M] = securityCodeM.split('.')

    getApp().sensors.track('PageClick', {
      first_module: '首页',
      second_module: '产业链图谱',
      third_module: '产业链图谱',
      page_name: '产业-产业链图谱',
      content_id: 'P000003',
      content_title: `${securityName}-产业链图谱`,
      page_version: '20201230',
      button_area: '产业相关公司',
      button_name: '行情',
      button_id: 'B00000004',
      button_title: `${securityName}-${code}`,
      stock_code: code,
      operating_activitie_name: '',
      operating_activitie_code: '',
      url: 'pages/industry/industry',
    });
    my.ap.navigateToAlipayPage({
      'appCode': "stockDetail",
      'appParams': {
        'market': M, //所在市场，SH-沪市，SZ-深，A\O\N\USI - 美股市场，HK-港股市场
        'symbol': code, //股票代码
        'name': securityName //股票名称
      },
      success: (res) => {
        console.log('成功：' + JSON.stringify(res));
      },
      fail: (res) => {
        console.log('失败：' + JSON.stringify(res));
      }
    });
  },
  // navigateBack() {
  //   console.log('.......')
  // }
});

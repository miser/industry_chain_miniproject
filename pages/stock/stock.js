import http from "/common/request";
import Api from "/common/api";
import { getMarketTag, open } from "/common/utils";
import colors, { NULL_COLOR } from "../../components/colors";
const globalData = getApp().globalData
import bury from "/common/bury"

Page({
  data: {
    helpShow: false,
    companyData: {},
    showExpand: false,
    isExpand: false,
    securityName: "",
    businessProducts: null,
    business: null,
    chainProducts: null,
    getActiveIndustry: () => { },
    routeActiveIndustry: () => { },
    activeIndustry: {}
  },
  onShow() {
    this._onShow && this._onShow();
    const { securityName, securityCode } = this.params;
    globalData.navList.push({
      securityName,
      securityCode
    })
  },
  onLoad(params) {
    const { securityName, securityCode } = params;

    const { onShow, onHide, onUnload } = bury.call(this, ({
      event: 'PageView',
      page_name: '个股-产业链图谱',
      content_id: 'P000002',
      content_title: `${securityName}-产业链图谱`,
      url: 'pages/stock/stock'
    }))


    this._onShow = onShow
    this.onHide = onHide
    this.onUnload = onUnload

    this.params = params
    my.setNavigationBar({
      title: securityName + '-产业链图谱'
    });
    this.setData({
      securityName,
      routeActiveIndustry: this.routeActiveIndustry.bind(this),
      getActiveIndustry: this.getActiveIndustry.bind(this)
    });
    this.getCompanyBaseInfo(securityCode);
    this.getMainBusinessProducts(securityCode);
    this.getCompany(securityCode)
  },
  calcExpand() {
    var a = my
      .createSelectorQuery()
      .select(".introduce .inner")
      .boundingClientRect()
      .exec(res => {
        if (res[0].height > 130 / 2) {
          this.setData({
            showExpand: true
          });
        }
      });
  },
  toggleExpand() {
    this.setData({
      isExpand: !this.data.isExpand
    });
  },
  getCompanyBaseInfo(securityCode) {
    http
      .get(Api.getCompanyBaseInfo, { userId: true, securityCode })
      .then(res => {
        if (!res.data) return;
        const increase = Number(res.data.increase);
        let color;
        if (increase > 0) {
          color = "red";
          res.data.increase = "+" + res.data.increase;
        }
        if (increase < 0) {
          color = "green";
          res.data.increase = res.data.increase;
        }
        if (increase == 0) {
          color = "normal";
        }
        res.data.securityCode = `${res.data.securityCode}.${getMarketTag(
          res.data.securityCode
        )}`;
        this.setData({
          companyData: res.data,
          color
        });
      });
  },
  getMainBusinessProducts(securityCode) {
    // getChainProducts
    const pChain = http.get(Api.getChainProducts, {
      userId: true,
      securityCode
    });

    const pBusiness = http.get(Api.getMainBusinessProducts, {
      userId: true,
      securityCode
    });

    Promise.all([pChain, pBusiness]).then(resArr => {
      const chainProducts = resArr[0];
      const businessProducts = resArr[1];

      if (!chainProducts.data || !businessProducts.data) return;

      const chainProductsFilter = chainProducts.data.map(item => {
        let color;
        businessProducts.data.forEach((element, index) => {
          if (element.financeCode === item.financeCode) {
            color = colors[index];
          }
        });
        if (!color) color = NULL_COLOR;
        return { ...item, color };
      });

      let prodList = businessProducts.data;
      let mainRatio = prodList.slice(0, 4).reduce((a, b) => {
        return a + b.ratio;
      }, 0);
      this.setData({
        businessProducts: prodList.slice(0, 4),
        restList: prodList.slice(4),
        restPercent: 100 - mainRatio,
        chainProducts: chainProductsFilter
      });
    });
  },
  fetchSub(item) {
    const productCode = item.productCode;
    return http
      .get(Api.getUpAndDownChain, { userId: true, productCode, loading: false })
      .then(res => {
        // console.log(res)
        Object.keys(res.data).forEach(key => {
          res.data[key].forEach(item => {
            try {
              // item.increase = item.increase.toFixed(2);
              if (item.increase > 0) {
                item.color = "red";
                item.extText = item.increase // ("+" + (item.increase * 100).toFixed(2)) + '%';
              } else if (item.increase < 0) {
                item.color = "green";
                item.extText = item.increase // (item.increase * 100).toFixed(2) + '%';
              } else {
                item.increase = null;
                item.extText = "";
              }
            } catch (e) {
              console.log(e);
            }
          });
        });
        return res;
      });
  },
  getActiveIndustry(item) {
    this.setData({
      activeIndustry: item
    });
  },
  routeActiveIndustry(item) {
    const { productName, productCode, increase } = item
    globalData.navList.push(item)
    open({
      url: `/pages/industry/industry?productName=${productName}&productCode=${productCode}&increase=${increase || ''}`
    })
  },
  goIndustry() {
    const { productName, productCode, increase } = this.data.activeIndustry;
    if (!productCode) return
    globalData.navList.push(this.data.activeIndustry)
    open({
      url: `/pages/industry/industry?productName=${productName}&productCode=${productCode}&increase=${increase || ''}`
    });
  },
  showHelp() {
    this.setData({
      helpShow: true
    });
  },
  setShow(flag) {
    this.setData({
      helpShow: flag
    });
  },
  getCompany(securityCode) {
    const self = this
    const market = getMarketTag(securityCode)

    my.request({
      url: Api.getCompany,
      method: "GET",
      data: { code: securityCode, market },
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success(res) {
        if (!res.data) {
          return;
        }
        const data = res.data
        if (data.result[0].code === 0) {
          if (data.brief.length > 0 && data.brief[0].business) {
            self.setData({
              business: data.brief[0].business
            })
            self.calcExpand()
          }
        }
      }
      // ,
      // fail(err) {
      //   reject(err);
      // }
    });
  },
  goMarket() {
    const { securityName, securityCodeM } = this.data.companyData;
    const [code, M] = securityCodeM.split('.')

    my.ap.navigateToAlipayPage({
      'appCode': "stockDetail",
      'appParams': {
        'market': M, //所在市场，SH-沪市，SZ-深，A\O\N\USI - 美股市场，HK-港股市场
        'symbol': code, //股票代码
        'name': securityName //股票名称
      },
      success: (res) => {
        console.log('成功：' + JSON.stringify(res));
      },
      fail: (res) => {
        console.log('失败：' + JSON.stringify(res));
      }
    });
  }
});

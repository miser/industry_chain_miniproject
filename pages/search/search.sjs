function handleRestul(item, searchValue) {
  var name = item.productName || item.securityName
  if (!searchValue) return [name]
  var reg = getRegExp('(' + searchValue + ')', 'i')
  var keyword = name.match(reg)
  if (keyword) {
    return name.split(reg)
  } else {
    return [name]
  }
}
export default { handleRestul: handleRestul }
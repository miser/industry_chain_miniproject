import http from "/common/request";
import Api from "/common/api";
import { debounce, open } from "/common/utils";
const globalData = getApp().globalData
Page({
  data: {
    searchValue: "",
    historyList: [],
    productChainList: [],
    companyList: [],
    showEmpty: 0
  },
  onLoad() {
    this.getHistoryList();
  },
  doSearch: debounce(function () {
    const { searchValue: keyword } = this.data;
    if (!keyword) {
      this.setData({
        productChainList: [],
        companyList: []
      });
      return;
    }
    http
      .get(Api.search, { keyword, userId: true })
      .then(res => {
        const {
          productChain: productChainList,
          company: companyList
        } = res.data;
        this.setData({
          productChainList,
          companyList,
          showEmpty: !(productChainList.length + companyList.length)
        });
      })
      .catch(() => {
        this.setData({
          productChainList: [],
          companyList: [],
          showResult: 0
        });
      });
  }),
  addHistory(params) {
    const { code, name, type } = params;
    return http.post(Api.addSearchHistory, { code, name, type, userId: true });
  },
  getHistoryList() {
    http.get(Api.getSearchHistory, { userId: true }).then(res => {
      // console.log(res.data)
      const filtList = res.data.map(item => item.name)//Array.from(new Set(res.data.map(item => item.name)));
      this.setData({ historyList: filtList });
    });
  },
  onInput(e) {
    const searchValue = e.detail.value;
    if (!searchValue) {
      this.getHistoryList();
    }
    this.setData({
      searchValue
    });
    this.doSearch();
  },
  async goDetail(e) {
    const { index, item } = e.target.dataset;
    let params = {};
    globalData.navList.splice(0, globalData.navList.length)
    if (index) {
      params = {
        code: item.securityCode,
        name: item.securityName,
        increase: item.increase,
        type: "company"
      };
      await this.addHistory(params);
      // globalData.navList.push(item)
      open({
        url: `/pages/stock/stock?securityName=${params.name}&securityCode=${params.code}`
      });
    } else {
      params = {
        code: item.productCode,
        name: item.productName,
        increase: item.increase,
        type: "productChain"
      };
      await this.addHistory(params);
      globalData.navList.push(item)
      open({
        url: `/pages/industry/industry?productName=${params.name}&productCode=${params.code}&increase=${params.increase}`
      });
    }
  },
  tapHistory(e) {
    this.setData({
      searchValue: e.target.dataset.keyword
    });
    this.doSearch();
  },
  clean() {
    http.post(Api.dropSearchHistory, { userId: true }).then(res => {
      this.setData({ historyList: [] });
    });
  }
});

const http = {};

//const baseURL = "https://guoxinindustrychain.aiyechen.com/gsnews/industrychain";
const baseURL = 'https://goldsunface.guosen.com.cn:1445/gsnews/industrychainmini/';
let methods = ["get", "post"];
let ajaxCount = 0;
for (const method of methods) {
  http[method] = (url, data = {}) => {
    const { userId, loading = true } = data
    if (userId) {
      const _userId = getApp().globalData.userId;
      console.log(`_userId => ${url}  => ${_userId}`)
      data.userId = _userId;
    }
    return new Promise(function (resolve, reject) {
      if (!ajaxCount && loading) {
        my.showLoading();
      }
      loading && ajaxCount++;
      let reqUrl = /http(s?):\/\//.test(url) ? url : baseURL + url;
      
      my.request({
        url: reqUrl,
        method: method.toUpperCase(),
        data,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success(res) {
          if (!res.data) {
            reject(res);
            return;
          }
          if (res.data.retCode === "000") {
            resolve(res.data);
          } else if (res.data.retMsg) {
            my.showToast({ content: res.data.retMsg });
            reject(res.data.retCode);
          } else {
            resolve(res.data);
          }
        },
        fail(err) {
          reject(err);
        },
        complete: function (res) {
          loading && ajaxCount--;
          if (!ajaxCount) {
            my.hideLoading();
          }
        }
      });
    }).catch(console.log);
  };
}

export default http;

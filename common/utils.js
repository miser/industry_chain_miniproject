export function debounce(fn, wait = 500) {
  let timer = null;
  return function (...args) {
    let context = this;
    if (timer) clearTimeout(timer);
    timer = setTimeout(() => {
      fn.apply(context, args);
    }, wait);
  };
}
export function open(option) {
  if (getCurrentPages().length > 5) {
    my.redirectTo(option);
  } else {
    my.navigateTo(option);
  }
}
export function mul(arg1, arg2) {
  var m = 0, s1 = arg1.toString(), s2 = arg2.toString();
  m += (s1.split(".")[1] || '').length
  m += (s2.split(".")[1] || '').length
  const r = Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m)
  return r > 0 ? '+' + r : r//Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m)
}

export function getMarketTag(code) {
  // code: "601318"
  // 1是深圳 2是上海
  // code 股票代码，market 市场标志(SZ-深圳(股票代码非6开头)，
  // SH-上海(股票代码6开头))
  if (code[0] === "6") {
    return "SH";
  }
  return "SZ";
}
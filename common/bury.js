
function calculateDuration(entryDate) {
  const currentDate = Date.now()
  const event_duration = currentDate - entryDate;
  const duration = (event_duration / 1000).toFixed(3)
  return duration
}

function bury({
  event,
  page_name,
  content_id,
  content_title,
  stock_code,
  url,
  event_duration
}) {
  event = event || 'PageClick'
  stock_code = stock_code || ''
  console.log({
    event,
    page_name,
    content_id,
    content_title,
    stock_code,
    url,
    event_duration
  })
  getApp().sensors.track(event, {
    first_module: '首页',
    second_module: '产业链图谱',
    third_module: '产业链图谱',
    page_name,
    content_id,
    content_title,
    page_version: '20201230',
    // button_area: '顶部',
    // button_name: '搜索',
    // button_id: 'B00000002',
    // button_title: '搜索',
    stock_code,
    operating_activitie_name: '',
    operating_activitie_code: '',
    url,
    '$event_duration': event_duration
  });
}

const BuryPoint = function ({
  event,
  page_name,
  content_id,
  content_title,
  stock_code,
  url
}) {
  let hasUsed = false

  return {
    onShow() {
      // 页面显示
      hasUsed = false
      this.entryDate = Date.now()
    },
    onHide() {
      hasUsed = true
      const duration = calculateDuration(this.entryDate)
      console.log(`BuryPoint onHide:${event}`)
      bury({
        event,
        page_name,
        content_id,
        content_title,
        stock_code,
        url,
        event_duration: duration
      })
      console.log(`onHide ${page_name}`)
    },
    onUnload() {
      if (hasUsed === true) {
        return
      }
      const duration = calculateDuration(this.entryDate)
      bury({
        event,
        page_name,
        content_id,
        content_title,
        stock_code,
        url,
        event_duration: duration
      })
      console.log(`onUnload ${page_name}`)
    },
  }
}

export default BuryPoint
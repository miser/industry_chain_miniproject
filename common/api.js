const api = {
  getHotList: "/product/queryHotQuotesChain",
  getHotCompanys: "/company/queryHotCompanys",
  search: "/search",
  addSearchHistory: "/addSearchHistory",
  getSearchHistory: "/querySearchHistory",
  dropSearchHistory: "/dropSearchHistory",
  getRelatedCompanys: "/company/queryRelatedCompanys",
  getCompanyBaseInfo: "/company/queryCompanyBaseInfo",
  getMainBusinessProducts: "/product/queryMainBusinessProducts",
  getChainProducts: "/product/queryChainProducts",
  getUpAndDownChain: "/product/queryUpAndDownChain",
  getUserId: "/queryAlipayUserId",
  getCompany: "https://goldsundg.guosen.com.cn:1445/gsnews/gsf10/company/brief/1.0",

  // 埋点地址
  // 测试地址  'https://sensor.guosen.com.cn/sa?project=default',//
  // 生产：https://sensor.guosen.com.cn/sa?project=production
  saUrl: "https://sensor.guosen.com.cn/sa?project=production"
};
export default api;

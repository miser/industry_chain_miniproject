function mul(arg1, arg2) {
  var m = 0, s1 = arg1.toString(), s2 = arg2.toString();
  m += (s1.split(".")[1] || '').length
  m += (s2.split(".")[1] || '').length
  return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m)
}
export default { mul: mul }
export const mainDataOrigin = [
  {
    "productCode": 1140077783,
    "financeCode": "124600696",
    "productName": "大宗商品贸易"
  },
  {
    "productCode": 1140065208,
    "financeCode": "224600696",
    "productName": "商业保理业务"
  },
  {
    "productCode": 1140098765,
    "financeCode": "324600696",
    "productName": "白酒销售"
  },
  {
    "productCode": 1140061102,
    "financeCode": "424600696",
    "productName": "房屋租赁"
  },
  {
    "productCode": 1140006497,
    "financeCode": "424600696",
    "productName": "融资租赁业务"
  }
]

export const subDataOrigin = {
  1140077783: {
    "up": [
      {
        "productCode": 1140059784,
        "productName": "营销网络营销网络营销网络营销网络营销网络营销网络营销网络营销网络"
      },
      {
        "productCode": 1140059784,
        "productName": "营销网络营销网络营销网络营销网络"
      },
      {
        "productCode": 1140059784,
        "productName": "营销网络营销网络",
        "extText": "+0.98",
        "color": "red"
      },
      {
        "productCode": 1140059784,
        "productName": "营销网络营销网络营",
        "extText": "+0.98",
        "color": "red"
      },
      {
        "productCode": 1140059784,
        "productName": "营销网络营销网络营销网络营销网络",
        "extText": "+0.98",
        "color": "green"
      },
      {
        "productCode": 1140059784,
        "productName": "营销网络营",
        "extText": "+0.98"
      },
      {
        "productCode": 1140045059,
        "productName": "大宗商品"
      }
    ],
    "down": [
      {
        "productCode": 1140043544,
        "productName": "物流运输"
      },
      {
        "productCode": 1140043544,
        "productName": "物流运输物流运输物流运输物流运输"
      },
      {
        "productCode": 1140043544,
        "productName": "物流运输物流运输物流运输物流运输",
        "extText": "+0.98",
        "color": "red"
      },
      {
        "productCode": 1140045640,
        "productName": "储备仓库"
      },
      {
        "productCode": 1140060584,
        "productName": "海关"
      },
      {
        "productCode": 1140045110,
        "productName": "航运",
        "extText": "+0.98",
        "color": "green"
      }
    ]
  },
  1140065208: {
    "up": [
      {
        "productCode": 1140026177,
        "productName": "信用风险管理"
      },
      {
        "productCode": 1140026178,
        "productName": "应收账款管理"
      },
      {
        "productCode": 1140087308,
        "productName": "应收账款催收"
      },
      {
        "productCode": 1140087309,
        "productName": "坏账担保"
      },
      {
        "productCode": 1140087310,
        "productName": "保理融资"
      }, {
        "productCode": 1140026177,
        "productName": "信用风险管理2"
      },
      {
        "productCode": 1140026178,
        "productName": "应收账款管理2"
      },
      {
        "productCode": 1140087308,
        "productName": "应收账款催收2"
      },
      {
        "productCode": 1140087309,
        "productName": "坏账担保2"
      },
      {
        "productCode": 1140087310,
        "productName": "保理融资2"
      }
    ],
    "down": [
      {
        "productCode": 1140107413,
        "productName": "投保公司"
      },
      {
        "productCode": 1140061346,
        "productName": "投保人"
      }
    ]
  }
}

import { backLine, oneLine } from '../../utils'
import { mul } from '/common/utils'
// import { subDataOrigin, mainDataOrigin } from '../../mock'
import G from '../../g'

function calculateHeight(els) {
  if (els.length === 0) return 0
  const lastEl = els[els.length - 1]
  return lastEl.top + lastEl.height;
}

var Point = function (x, y) {
  return { x: x, y: y };
};

function createElementClasses(calculate, measureText) {
  class Text {
    constructor(options = {}) {
      if (options.text === null || options.text === undefined) {
        throw new Error('must assign a value to the text of options')
      }
      this.text = options.text
      this.width = options.width || measureText(this.text).width
      this.height = options.height
      this.baseline = options.baseline

      this.color = options.color || ''
      this.offset = options.offset || { x: 0, y: 0 }
    }
    // baseline 
    // calculate() {
    //   this.x = this.baseline.x + this.padding
    //   this.y = this.baseline.x + this.padding
    // }
  }

  return {
    Text
  }
}


function drawRoundedRect(rect, r, ctx) {
  var ptA = Point(rect.x + r, rect.y);
  var ptB = Point(rect.x + rect.width, rect.y);
  var ptC = Point(rect.x + rect.width, rect.y + rect.height);
  var ptD = Point(rect.x, rect.y + rect.height);
  var ptE = Point(rect.x, rect.y);

  ctx.moveTo(ptA.x, ptA.y);
  ctx.arcTo(ptB.x, ptB.y, ptC.x, ptC.y, r);
  ctx.arcTo(ptC.x, ptC.y, ptD.x, ptD.y, r);
  ctx.arcTo(ptD.x, ptD.y, ptE.x, ptE.y, r);
  ctx.arcTo(ptE.x, ptE.y, ptA.x, ptA.y, r);
}

class Graph extends G {
  constructor(canvas, opts) {
    super(canvas, opts)
    this.MAIN = {
      LR_PADDING: this.calculate(16),
      ROUND: this.calculate(8),
      TOP_MARGIN: this.calculate(20)
    }

    this.SUB = {
      ROUND: this.calculate(8),
      RECT_MAX_WIDTH: this.calculate(181),
      RECT_LINE_WIDTH: this.calculate(1),
      TWO_RECT_T_PADDING: this.calculate(24),
      LR_PADDING: this.calculate(8),
      TB_PADDING: this.calculate(8),
      TEXT_HEIGHT: this.calculate(30),
      EXT_PADDING: this.calculate(2)
    }

    // this.mainDataOrigin = mainDataOrigin
    // this.subDataOrigin = subDataOrigin

    const classFactory = createElementClasses(this.calculate, this.measureText)
    this.Text = classFactory.Text

    this.init()
    // this.setFocusData(this.mainDataOrigin[0])
  }

  init() {
    // this.width = this.canvas.width // this.ratio
    // this.height = 0
    this.elements = []
    this.leftEls = []
    this.rightEls = []
    this.midEls = []

  }

  setFocusData(data) {
    this.focusData = { ...data }
  }

  setMainDataOrigin(data) {
    this.mainDataOrigin = data
  }

  setSubDataOrigin(data) {
    this.subDataOrigin = data
  }

  drawMiddle() {
    const { canvas, MAIN, calculate, CANVAS_MIDDLE, mainDataOrigin, focusData, elements, midEls } = this
    let width = calculate(168);
    let height = calculate(70);
    let pedding = calculate(12);
    canvas.fontSize = calculate(this.getFontSize(24))
    mainDataOrigin.forEach((item, index) => {
      // 
      const t = oneLine(this.measureText, item.productName, width, '', pedding)

      const el = {
        type: 'main', height, width, text: t, item,
        top: (height + MAIN.TOP_MARGIN) * index,
        left: CANVAS_MIDDLE - width / 2,
        increase: item.increase
      }
      if (focusData.productCode === item.productCode) focusData.el = el
      elements.push(el)
      midEls.push(el)
    })
  }

  drawLine() {
    const { canvas, focusData, leftEls, rightEls, elements } = this
    canvas.setLineDash([2, 4]);
    const mainElLeftX = focusData.el.left
    const mainElRightX = focusData.el.left + focusData.el.width
    const mainElMiddleY = focusData.el.top + (focusData.el.height) / 2

    canvas.strokeStyle = focusData.color;

    if (leftEls.length > 0) {
      let topY = mainElMiddleY
      let lastY = 0
      const middleX = leftEls[0].left + leftEls[0].width + (mainElLeftX - (leftEls[0].left + leftEls[0].width)) / 2
      canvas.beginPath();
      canvas.moveTo(middleX, mainElMiddleY);
      canvas.lineTo(mainElLeftX, mainElMiddleY);
      canvas.closePath();
      canvas.stroke();

      leftEls.forEach((el, i) => {
        canvas.beginPath();
        lastY = el.top + (el.height) / 2
        canvas.moveTo(el.left + el.width, lastY);
        canvas.lineTo(middleX, lastY);
        topY = Math.min(topY, lastY)
        canvas.closePath();
        canvas.stroke();
      })

      canvas.beginPath();
      canvas.moveTo(middleX, topY);
      canvas.lineTo(middleX, Math.max(lastY, mainElMiddleY))
      canvas.closePath();
      canvas.stroke();

    }
    if (rightEls.length > 0) {
      let topY = mainElMiddleY
      let lastY = 0
      const middleX = mainElRightX + (rightEls[0].left - mainElRightX) / 2

      canvas.beginPath();
      canvas.moveTo(middleX, mainElMiddleY);
      canvas.lineTo(mainElRightX, mainElMiddleY);
      canvas.closePath();
      canvas.stroke();

      rightEls.forEach(el => {
        canvas.beginPath();
        lastY = el.top + (el.height) / 2
        canvas.moveTo(el.left, lastY);
        canvas.lineTo(middleX, lastY);
        topY = Math.min(topY, lastY)
        canvas.closePath();
        canvas.stroke();
      })

      canvas.beginPath();
      canvas.moveTo(middleX, topY);
      canvas.lineTo(middleX, Math.max(lastY, mainElMiddleY));
      canvas.closePath();
      canvas.stroke();
    }

    canvas.setLineDash([])
  }

  draw() {
    const { elements, canvas, focusData, CANVAS_MIDDLE, MAIN, SUB } = this
    elements.forEach(el => {
      canvas.beginPath();

      // canvas.font = `${calculate(12)}px`
      canvas.textAlign = 'center'
      canvas.textBaseline = "alphabetic"
      if (el.type == 'main') {
        canvas.fillStyle = el.item.color;
        canvas.strokeStyle = el.item.color;
        drawRoundedRect({ x: el.left, y: el.top, width: el.width, height: el.height }, MAIN.ROUND, canvas)
        canvas.fill()

        canvas.fillStyle = '#FFFFFF'
        if (el.increase === 0) {
          canvas.fillText(el.text, CANVAS_MIDDLE, el.top + el.height / 1.5);
        } else {
          canvas.fillText(el.text, CANVAS_MIDDLE, el.top + el.height / 2.2);
          canvas.fillText(`${(el.increase * 100).toFixed(2)}%`, CANVAS_MIDDLE, el.top + el.height / 1.2);
        }
      } else {
        canvas.fillStyle = '#000000'
        canvas.textAlign = 'start'
        canvas.textBaseline = "top"

        el.textEls.forEach(txt => {
          canvas.fillText(txt.text, el.left + txt.offset.x, el.top + txt.offset.y)
        })
        if (el.extText.text) {
          canvas.fillStyle = el.extText.color || '#000000'
          const lastTxt = el.textEls[el.textEls.length - 1]
          canvas.fillText(el.extText.text, el.left + lastTxt.offset.x + lastTxt.width + SUB.EXT_PADDING, el.top + lastTxt.offset.y)
        }
        canvas.strokeStyle = focusData.color
        drawRoundedRect({ x: el.left, y: el.top, width: el.width, height: el.height }, SUB.ROUND, canvas)
      }
      canvas.lineWidth = this.calculate(2);
      canvas.closePath();
      canvas.stroke()
    })

    canvas.draw();
  }

  render() {
    const { canvas, midEls, subDataOrigin, focusData,
      calculate, elements, leftEls, rightEls, SUB, MAIN, Text } = this
    this.drawMiddle()
    const measureText = canvas.measureText.bind(canvas)
    const subData = subDataOrigin


    Object.keys(subData).forEach(key => {
      const data = subData[key]
      let preX = calculate(16);

      data.forEach((item, index) => {
        const text = item.productName;
        // const extText = item.extText
        const extText = item.extText && mul(item.extText, 100) + '%';
        let txtInfo = backLine(measureText, text, SUB.RECT_MAX_WIDTH - SUB.LR_PADDING * 2, extText, SUB.EXT_PADDING)
        const txtArr = txtInfo.els
        const textEls = []
        let startTop = SUB.TB_PADDING

        txtArr.forEach(txt => {
          if (txt === null) return
          const el = new Text({
            text: txt.text,
            width: txt.width,
            height: SUB.TEXT_HEIGHT,
            offset: { x: SUB.LR_PADDING, y: startTop }
          })
          startTop = startTop + SUB.TEXT_HEIGHT
          textEls.push(el)
        })
        const lastTextEl = textEls[textEls.length - 1]
        const height = lastTextEl.offset.y + lastTextEl.height + SUB.TB_PADDING
        const width = txtInfo.width + SUB.LR_PADDING * 2
        let el
        if (index === 0 && txtArr.length === 2 && txtArr[1] === null) {
          preX = (elements[0].top + elements[0].height) / 2 - height / 2
        }
        if (key === 'up') {
          el = {
            type: key,
            height,
            width,
            textEls: textEls,
            extText: {
              text: extText,
              color: item.color
            },
            item,
            top: preX,
            left: MAIN.LR_PADDING + SUB.RECT_MAX_WIDTH - width
          }
          leftEls.push(el);
        } else {
          el = {
            type: key,
            height,
            width,
            textEls: textEls,
            extText: {
              text: extText,
              color: item.color
            },
            item,
            top: preX,
            left: this.width - MAIN.LR_PADDING - SUB.RECT_MAX_WIDTH
          };
          rightEls.push(el);
        }
        preX += height + SUB.TWO_RECT_T_PADDING
        elements.push(el)
      })
    })

    this.drawLine()
    this.draw()
    canvas.height = Math.max(calculateHeight(midEls), calculateHeight(leftEls), calculateHeight(rightEls)) + calculate(30)
    this.height = canvas.height
  }

  clear() {
    const { canvas } = this
    canvas.clearRect(0, 0, canvas.width, canvas.height)
    this.init()
  }

  destroy() {
    // this.svg.remove();
  }
}

export default Graph
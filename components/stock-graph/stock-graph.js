import Graph from './lib'

Component({
  mixins: [],
  data: {
    width: 0,
    height: 0,
    mainList: null,
    subList: null,
    canvasWidth: 0,
    canvasHeight: 0,
    isReRender: true
  },
  props: {
    data: null,
    onFetchSub: null,
    onActiveIndustry: null,
    onRoute: null,
  },
  didMount() {
    const info = my.getSystemInfoSync()
    const ratio = info.pixelRatio
    this.ratio = ratio

    // this.setData({
    //   canvasHeight: 1000, //canvas.height / ratio,
    //   height: 2000//canvas.height
    // })

    my.createSelectorQuery().select('#canvas-wrap').boundingClientRect().exec(rects => {
      const res = rects[0]
      this.canvas = my.createCanvasContext('canvas');

      let w = res.width//calculate(ratio)(res.width)

      let _w = ratio * w// res.width;
      let _h = ratio * res.height;

      this.canvas.width = _w

      this.graph = this.graph = new Graph(this.canvas,
        { width: _w, ratio, appInfo: info })

      this.setData({
        width: _w,
        canvasWidth: w
      });
    })
  },
  deriveDataFromProps(nextProps) {
    if (nextProps.data !== this.data.mainList) {
      this.setData({
        mainList: nextProps.data
      });
      this.props.onFetchSub(nextProps.data[0]).then(res => {
        setTimeout(() => {
          this.graph.setFocusData(nextProps.data[0])
          this.props.onActiveIndustry(nextProps.data[0])
          this.setData({ subList: res.data })
        }, 100)
      })
    }
  },
  didUpdate() {
    if (this.data.isReRender === true &&
      Array.isArray(this.data.mainList) &&
      this.data.subList &&
      this.data.width !== 0) {
      this.rerender()
    }
  },
  didUnmount() { },
  methods: {
    tapCanvas(event) {
      const _this = this
      const { graph, canvas, ratio, props, clear, rerender } = this;
      const { elements } = graph

      const q = my.createSelectorQuery().selectViewport().scrollOffset()
      q.exec(function (res) {
        const scrollTop = res[0].scrollTop

        const { x, y } = event.detail
        const xx = x * ratio
        // const yy = (scrollTop + y) * ratio
        const yy = y * ratio //(scrollTop + y) * ratio

        elements.forEach((element) => {
          if (
            xx > element.left &&
            xx < element.left + element.width &&
            yy > element.top &&
            yy < element.top + element.height
          ) {
            if (element.type === "main") {
              _this.clear()
              _this.graph.setFocusData(element.item)
              _this.props.onActiveIndustry(element.item)
              props.onFetchSub(element.item).then(res => {
                _this.setData({ subList: res.data })
              })
            } else {
              _this.props.onRoute(element.item)
            }
          }
        });
      })
    },
    rerender() {
      this.graph.setMainDataOrigin(this.data.mainList)
      this.graph.setSubDataOrigin(this.data.subList)
      this.graph.render()
      this.setData({
        isReRender: false,
        canvasHeight: this.canvas.height / this.ratio,
        height: this.canvas.height
      })
    },
    clear() {
      this.setData({
        isReRender: true,
        subList: null
      })
      const { graph } = this;
      graph.clear()
    },
  },
});

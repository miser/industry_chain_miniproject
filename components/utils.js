export function calculate(ratio) {
  const windowWidth = my.getSystemInfoSync().windowWidth
  const scale = windowWidth / 750
  const map = {}
  return (x) => {
    if (map[x]) return map[x]
    map[x] = scale * x * ratio
    return map[x]
    // return Math.floor(windowWidth * x / 750)
  }
}

export function getFontSize(appInfo) {
  let scale = 1
  if (appInfo.platform === 'ios') {
    scale = 0.62
  }
  return (x) => {
    return x * scale
  }
}

export function backLine(measureText, text, maxWidth, extText, padding) {
  let tw = measureText(text).width
  let extTW = measureText(extText).width
  if (extTW === 0 || Number.isNaN(extTW) || extTW === undefined) {
    extTW = 0
  } else {
    extTW = extTW + padding
  }
  let leaveStr = ''
  let wholeWidth = 0
  let els;
  while (tw > maxWidth) {
    leaveStr = text.substring(text.length - 1) + leaveStr
    text = text.substring(0, text.length - 1)
    tw = measureText(text).width
  }

  // 一行数据正好
  if (leaveStr.length === 0) {
    // extText无法存放
    if (tw + extTW > maxWidth) {
      wholeWidth = maxWidth
      els = [{ text, width: tw }, { text: '', width: 0 }]
    } else {
      wholeWidth = tw + extTW
      els = [{ text, width: tw }, null]
    }
    return {
      width: wholeWidth,
      els
    }
  } else {
    wholeWidth = maxWidth
    let newMaxWidth = maxWidth - extTW
    let newTW = measureText(leaveStr).width
    let dotSymbol = '...'
    let dotWidth = measureText(dotSymbol).width
    let needDotSymbol = false
    if (newTW > newMaxWidth) {
      newMaxWidth = maxWidth - extTW - dotWidth
      needDotSymbol = true
    }
    while (newTW > newMaxWidth) {
      leaveStr = leaveStr.substring(0, leaveStr.length - 1)
      newTW = measureText(leaveStr).width
    }
    if (needDotSymbol) {
      leaveStr += dotSymbol
      newTW = newTW + dotWidth
    }
    els = [{ text, width: tw }, { text: leaveStr, width: newTW }]
    return {
      width: wholeWidth,
      els
    }
  }
}

export function oneLine(measureText, text, maxWidth, extText, padding) {
  // 
  const paddingWidth = padding
  const maxW = maxWidth - paddingWidth * 2
  let tw = measureText(text).width
  let currentText = text
  let dotSymbol = '...'
  let dotWidth = measureText(dotSymbol).width
  while (tw > maxW) {
    currentText = currentText.substring(0, currentText.length - 1)
    tw = measureText(currentText).width + dotWidth
  }

  if (currentText !== text) {
    currentText += dotSymbol
  }
  return currentText
}
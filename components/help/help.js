Component({
  data: { isShow: false },
  props: {
    type: String,//industry|data
    onSetShow: (flag) => { }
  },

  methods: {
    close() {
      this.props.onSetShow(false)
    },
    preventD() {
      return false
    }
  },
});

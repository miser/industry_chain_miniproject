import { calculate, getFontSize } from './utils'

class G {
  constructor(canvas, opts) {
    this.options = opts || {};

    this.ratio = this.options.ratio

    this.calculate = this.options.calculate || calculate(this.ratio);
    this.width = canvas.width
    const measureTextOrigin = canvas.measureText
    this.measureText = function (text) {
      const w = measureTextOrigin.call(canvas, text).width
      if (w === 0 || Number.isNaN(w) || w === undefined) {
        return { width: 0 }
      } else {
        return { width: w }
      }
    }

    this.CANVAS_MIDDLE = this.width / 2
    this.appInfo = this.options.appInfo
    this.getFontSize = getFontSize(this.appInfo)
    this.canvas = canvas
  }
  clear() {
    const { canvas } = this
    canvas.clearRect(0, 0, canvas.width, canvas.height)
    this.init()
  }

  destroy() {

  }
}

export default G
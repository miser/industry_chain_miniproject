import http from "/common/request";
import Api from "/common/api";
Component({
  mixins: [],
  data: { helpShow: false, upList: [], downList: [], upExpand: false, downExpand: false },
  props: {
    upAndDownData: {},
    productName: '',
    increase:'',
    upAndDownHandle: ''
  },
  didMount() {
    this.setData({
      upList: this.props.upAndDownData.up.slice(0, 9),
      downList: this.props.upAndDownData.down.slice(0, 9),
      upExpand: false,
      downExpand: false
    })
  },
  didUpdate(prevProps, prevData) {
    if (prevProps.productName !== this.props.productName) {
      this.setData({
        upList: this.props.upAndDownData.up.slice(0, 9),
        downList: this.props.upAndDownData.down.slice(0, 9),
        upExpand: false,
        downExpand: false
      })

    }
  },
  didUnmount() { },
  deriveDataFromProps(nextProps) {

  },
  methods: {
    upAndDownHandle(e) {
      this.props.upAndDownHandle(e);
    },
    showHelp() {
      this.setData({
        helpShow: true
      });
    },
    setShow(flag) {
      this.setData({
        helpShow: flag
      });
    },
    toggleUpExpand() {
      const upExpand = !this.data.upExpand
      let upList = []
      if (upExpand) {
        upList = this.props.upAndDownData.up
      } else {
        upList = this.props.upAndDownData.up.slice(0, 9)
      }
      this.setData({
        upExpand,
        upList
      })
    },
    toggleDownExpand() {
      const downExpand = !this.data.downExpand
      let downList = []
      if (downExpand) {
        downList = this.props.upAndDownData.down
      } else {
        downList = this.props.upAndDownData.down.slice(0, 9)
      }
      this.setData({
        downExpand,
        downList
      })
    },
  }
});

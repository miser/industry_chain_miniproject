import Pie from './lib/index'
import colors from '../colors'

Component({
  data: {
    width: 0,
    height: 0,
    canvasWidth: 0,
    canvasHeight: 0,
    list: null,
    colors
  },
  props: {
    data: null,
    title: ""
  },
  didMount() {
    this.isReRender = false
    const info = my.getSystemInfoSync()
    const ratio = info.pixelRatio
    this.ratio = ratio

    my.createSelectorQuery().select('.pie').boundingClientRect().exec(rects => {
      const res = rects[0]
      this.canvas = my.createCanvasContext('canvas-pie');
      let _w = ratio * res.width
      let _h = ratio * res.height;
      this.canvas.width = _w
      this.canvas.height = _h
      this.setData({
        width: _w,
        height: _h,
        canvasWidth: res.width,
        canvasHeight: res.height
      });

      const data = this.data.list.map(item => item.ratio)
      this.pie = new Pie(this.canvas,
        {
          data,
          width: _w,
          height: _h,
          text: this.props.title,
          ratio, appInfo: info
        })

      this.pie.render()
    }, 1000)
  },
  didUpdate() {
  },
  didUnmount() { },
  deriveDataFromProps(nextProps) {
    if (this.data.list === null && Array.isArray(nextProps.data)) {
      this.setData({
        list: nextProps.data
      });
    }
  },
  methods: {},
});

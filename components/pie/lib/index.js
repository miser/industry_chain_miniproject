import G from '../../g'
import colors from '../../colors'
import { backLine } from '../../utils'

class Pie extends G {
  constructor(canvas, opts) {
    super(canvas, opts)

    canvas.height = opts.height
    this.data = opts.data || []
    this.text = opts.text || ''
  }

  init() {

  }


  draw() {
    const { calculate, canvas, data, text } = this
    console.log(data)
    let total = 0;
    data.forEach(val => {
      total += val;
    });

    const paddingCount = data.length - 1;
    const padding = 0.004
    const p = total * padding;

    var angleList = [];
    data.forEach((val, i) => {
      var angle = Math.PI * 2 * (val / total);
      angleList.push(angle);
    });

    const w = canvas.width;
    const h = canvas.height;
    const x0 = (w - calculate(20)) / 2;
    const y0 = (h - calculate(20)) / 2;
    const r = calculate(77)
    const textWidth = calculate(48 - 28)
    const linewidth1 = calculate(58)
    const linewidth2 = calculate(48)

    let startAngle = 0;
    let offset = 2;
    angleList.forEach((item, i) => {
      var endAngle = startAngle + item;

      var medianAngle = (endAngle + startAngle) / 2;
      var offsetX = Math.cos(medianAngle) * offset;
      var offsetY = Math.sin(medianAngle) * offset;

      canvas.beginPath();
      canvas.arc(x0 + offsetX, y0 + offsetY, r, startAngle, endAngle);
      canvas.strokeStyle = colors[i]
      canvas.lineWidth = i === 0 ? linewidth1 : linewidth2;
      canvas.stroke();
      startAngle = endAngle;
      canvas.closePath();
    });

    const textArr = backLine(this.measureText, text, textWidth, '', 0)

    let offetY = calculate(-10)
    textArr.els.forEach(element => {
      if (!element) return
      canvas.beginPath();
      canvas.textAlign = 'center'
      canvas.textBaseline = "alphabetic"
      canvas.fillStyle = '#212634'
      canvas.fontSize = calculate(this.getFontSize(26))
      canvas.fillText(element.text, x0, y0 + offetY)
      offetY += calculate(30)
    })

    canvas.draw();
  }

  render() {
    this.draw()
  }

  clear() {
    const { canvas } = this
    canvas.clearRect(0, 0, canvas.width, canvas.height)
    this.init()
  }

  destroy() {
    // this.svg.remove();
  }
}

export default Pie


// HTML  JS Result  
// EDIT ON

//  function draw() {
//   var canvas = document.getElementById("canvas");
//   var ctx = canvas.getContext("2d");

//   var colors = ['#4CAF50', '#00BCD4', '#E91E63', '#FFC107', '#9E9E9E', '#CDDC39', '#18FFFF', '#F44336', '#FFF59D', '#6D4C41'];
//   var angles = [Math.PI * 0.3, Math.PI * 0.7, Math.PI * 0.2, Math.PI * 0.4, Math.PI * 0.4];
//   var offset = 10;
//   var beginAngle = 0;
//   var endAngle = 0;
//   var offsetX, offsetY, medianAngle;

//   for(var i = 0; i < angles.length; i = i + 1) {
//     beginAngle = endAngle;
//     endAngle = endAngle + angles[i];
//     medianAngle = (endAngle + beginAngle) / 2;
//     offsetX = Math.cos(medianAngle) * offset;
//     offsetY = Math.sin(medianAngle) * offset;
//     ctx.beginPath();
//     ctx.fillStyle = colors[i % colors.length];
//     ctx.moveTo(200 + offsetX, 200 + offsetY);
//     ctx.arc(200 + offsetX, 200 + offsetY, 120, beginAngle, endAngle);
//     ctx.lineTo(200 + offsetX, 200 + offsetY);
//     ctx.stroke();
//     ctx.fill();
//   }
// }

// window.onload = draw;
import http from "/common/request";
import Api from "/common/api";
import sa from "/common/sensorsdata.custom.min";
sa.setPara({
  server_url: Api.saUrl,
  // 是否允许控制台打印查看埋点数据(建议开启查看)
  show_log: true,
  appLaunch: false, // 默认为 true，false 则关闭 $MPLaunch 事件采集
  appShow: false, // 默认为 true，false 则关闭 $MPShow 事件采集
  appHide: false, // 默认为 true，false 则关闭 $MPHide 事件采集
  pageShow: false, // 默认为 true，false 则关闭 $MPViewScreen 事件采集
  pageShare: false, // 默认为 true，false 则关闭 $MPShare 事件采集
  mpClick: false, // 默认为 false，true 则开启 $MPClick 事件采集 
  mpFavorite: false // 默认为

});
sa.init();

App({
  onLaunch(options) {
    this.getAuthCode();

    // product_name = 支付宝小程序； 
    // platform_name = Android/iOS/H5/Windows 
    const _this = this;
    _this.sensors = sa;

    my.getSystemInfo({
      success: (res) => {
        const platform = res.platform
        const obj = {
          product_name: '支付宝小程序',
          platform_name: platform[0].toLocaleUpperCase() + platform.substring(1)
        }
        sa.registerApp(obj);
      }
    })
  },
  getAuthCode() {
    const _this = this;
    my.getAuthCode({
      scopes: "auth_base",
      success: (r) => {
        console.log(r.authCode)
        http.get(`${Api.getUserId}?authCode=${r.authCode}`).then(res => {
          if (!res) {
            return
          }
          _this.globalData.userId = res.data;
        });
      }
    });
  },
  globalData: {
    userId: 'ForErrorUserId001',
    navList: []
  }
});
